<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 26.07.2021
 * Time: 10:35
 */
class User
{
    private $name;
    private $age;
    private $email;


    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(string $name, int $age, string $email)
    {
        $this->setName($name);
        $this->setAge($age);
        $this->setEmail($email);

    }
    /* public function __call
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __call($name, $arguments)
    {
        echo "Вызов метода не существует: '$name' "
            . implode(', ', $arguments). "\n";
    }

    /* private function setName
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    private function setName($name)
    {
        $this->name = $name;
    }

    /* private function setAge
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    private function setAge($age)
    {
        $this->age = $age;
    }

    /* public function getAll
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAll(): array
    {
        return [
            'name' => $this->name,
            'age' => $this->age,
            'email' => $this->email,
        ];
    }
    
    
}

$obj = new User('Taras', 39, 'tchubarko@gmail.com');
echo '</br>';
echo sprintf('<pre>%s</pre>', print_r($obj->getAll(), true));