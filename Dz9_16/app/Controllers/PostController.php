<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 14:20
 */

namespace App\Controllers;


use App\Models\Post;
use Core\Controller;

class PostController extends Controller
{

    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        $posts = Post::q()->get();
        return $this->render('posts.index', ['posts' => $posts]);
    }
}