<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:13
 */

namespace App\Controllers;


use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Core\Controller;

class HomeController extends Controller
{
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        return $this->render('home');
    }

    /* public function install
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function install()
    {
        Post::createTable();
        User::createTable();

        $faker = \Faker\Factory::create();

        if(Post::q()->count() == 0){
            foreach(range(1, 30) as $i){
                Post::create([
                    'title' => $faker->sentence(),
                    'body' => $faker->text(),

                ]);
            }
        }

        if(User::q()->count() == 0){
            User::create([
                'login' => 'admin',
                'first_name' => 'Taras',
                'last_name' => 'Chubarko',
                'password' => password_hash(123456789, PASSWORD_BCRYPT),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        $_SESSION['msg'] = [
            'type' => 'success',
            'msg' => 'Установка прошла успешно.',
        ];

        header('Location: '.getenv('HOST'));
    }

    /* public function clearsession
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function clearsession()
    {
        unset($_SESSION[$_POST['ses']]);
        header('Location: '.getenv('HOST'));
    }
}