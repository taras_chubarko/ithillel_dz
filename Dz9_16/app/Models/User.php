<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 25.08.2021
 * Time: 12:46
 */

namespace App\Models;


use Core\Model;

class User extends Model
{
    protected static $table = 'users';

    /* public function createTable
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function createTable()
    {
        $query =  static::db()->prepare('CREATE TABLE IF NOT EXISTS users (
            id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT UNIQUE, 
            login VARCHAR(255) NOT NULL,
            first_name VARCHAR(80) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            password VARCHAR(255) NOT NULL,
            created_at TIMESTAMP NOT NULL,
            updated_at TIMESTAMP NOT NULL
            );');

        return $query->execute();
    }
}