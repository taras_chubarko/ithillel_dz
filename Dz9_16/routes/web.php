<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:15
 */

$router->setNamespace('\App\Controllers');


$router->get('/', 'HomeController@index');
$router->get('/articles', 'PostController@index');
$router->post('/install', 'HomeController@install');
$router->post('/clearsession', 'HomeController@clearsession');

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
});

