<div class="alert alert-warning" role="alert">
    <p>Перед установной создайте файл .env в корне сайта со своими параметрами</p><br>
    <p>HOST</p>
    <p>DB_HOST</p>
    <p>DB_NAME</p>
    <p>DB_USER</p>
    <p>DB_PASSWORD</p>
</div>

<div class="alert alert-primary" role="alert">
    <p>После установки для входа в админку используйте логин admin пароль 123456789</p>
</div>

<form method="post" action="{{getenv('HOST')}}/install">
    <button type="submit" class="btn btn-primary">Install</button>
</form>