@if($_SESSION['msg'])
    <div id="msg" class="alert alert-{{$_SESSION['msg']['type']}} mt-2" role="alert">
        <p>{{$_SESSION['msg']['msg']}}</p>
    </div>
    <script>
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "{{getenv('HOST')}}/clearsession",
                data: {
                    ses: 'msg'
                },
                success: function( result ) {
                    $('#msg').remove();
                }
            });
        }, 2000)
    </script>
@endif