<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:06
 */

namespace Core;

use Jenssegers\Blade\Blade;

class Controller
{
    protected $views;

    public function __construct()
    {
        $this->views = new Blade('resources/views', 'resources/cache');
    }

    /* public function render
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function render($view, $params = [])
    {
        echo $this->views->make($view, $params)->render();
    }
}