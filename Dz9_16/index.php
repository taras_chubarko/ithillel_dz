<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:02
 */
session_start();

require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/vendor/fzaninotto/faker/src/autoload.php';

use Core\Router;

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

$router = new Router();

require_once __DIR__ . '/routes/web.php';

$router->run();