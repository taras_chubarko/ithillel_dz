<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 26.07.2021
 * Time: 10:56
 */

class User
{
    private $id;
    private $password;

    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct($id, string $password)
    {
        $this->setId($id);
        $this->setPassword($password);
    }

    private function setId($id)
    {
        try {
            if (is_int($id))
                $this->id = $id;
            else
                throw new Exception('$id - должно быть целым числом');
        } catch (Exception $e) {
            echo sprintf('<pre>%s</pre>', print_r($e, true));
        }
    }

    private function setPassword($password)
    {
        try {
            if (strlen($password) >= 8)
                $this->password = $password;
            else
                throw new Exception('$password - должен быть больше 8 символов');
        } catch (Exception $e) {
            echo sprintf('<pre>%s</pre>', print_r($e, true));
        }
    }

}

$obj = new  User('1', '1234567');
echo sprintf('<pre>%s</pre>', print_r($obj, true));