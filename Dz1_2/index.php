<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.07.2021
 * Time: 9:54
 */

class Worker
{
    private $name;
    private $age;
    private $salary;

    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(string $name, int $age, float $salary)
    {
        if ($age <= 18) {
            throw new InvalidArgumentException('Рабочий может быть только полнолетним');
        }

        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

    /* public function getName
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getName(): string
    {
        return $this->name;
    }

    /* public function getAge
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /* public function getSalary
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}

$obj = new Worker('Jack', 25, 1000);
$age = $obj->getAge();
$salary = $obj->getSalary();

echo "возраст $age зарплата $salary";