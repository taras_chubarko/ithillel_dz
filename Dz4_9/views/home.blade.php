@extends('main')

@section('content')

    @if(!$if_table)
        @include('create_table')
    @else
        <div class="alert alert-warning mb-5" role="alert">
            Таблица Users уже создана
        </div>
        <div class="row">
            <div class="col-8">
                @include('users_table', ['users' => $users])
            </div>
            <div class="col-4">
                @include('user_add')
            </div>
        </div>
    @endif

@endsection