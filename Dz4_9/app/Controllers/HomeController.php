<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 03.08.2021
 * Time: 10:29
 */

namespace App\Controllers;
use Jenssegers\Blade\Blade;
use App\Models\User;

class HomeController
{

    protected $views;
    protected $user;
    public $location = 'http://localhost/ithillel_dz/Dz4_9';
    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct()
    {
        $this->views = new Blade('views', 'cache');
        $this->user = new User();
    }
    /* public function index
     * @param
     *-----------------------------------
     *|//echo sprintf('<pre>%s</pre>', print_r($users, true));
     *-----------------------------------
     */
    public function index()
    {
        $if_table = $this->user->tableExists();
        $users = $this->user->all();
        return $this->views->make('home', [
            'if_table' => $if_table,
            'users' => $users,
        ])->render();
    }

    /* public function createUsersTable
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function createUsersTable()
    {
        $this->user->createTable();
        header('Location: '.$this->location);
    }

    /* public function create
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create($request = [])
    {
        $item = $this->user->create($request);
        header('Location: '.$this->location);
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete(int $id)
    {
        $this->user->delete($id);
        header('Location: '.$this->location);
    }

    /* public function delete_selected
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete_selected($request = [])
    {
        foreach ($request['id'] as $item){
            $this->user->delete($item);
        }
        header('Location: '.$this->location);
    }

    /* public function view
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user(int $id = null)
    {
        if($id){
            $user = $this->user->find($id);
            echo sprintf('<pre>%s</pre>', print_r($user, true));
        }else{
            header('Location: '.$this->location);
        }

    }

}