<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 16:41
 */

namespace App\Classes;


interface DriverInterface
{
    public function amount($amount);
}