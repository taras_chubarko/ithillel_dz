<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 15:40
 */

namespace App\Controllers;


class HomeController
{
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        $result = [
            'controller' => 'HomeController',
            'action' => 'index',
            'params' => []
        ];
        dd($result);
    }

    /* public function show
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function show(int $id)
    {
        $result = [
            'controller' => 'HomeController',
            'action' => 'show',
            'params' => [
                'id' => $id
            ]
        ];
        dd($result);
    }
}