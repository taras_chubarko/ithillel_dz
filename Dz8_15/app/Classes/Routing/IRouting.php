<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 15:25
 */

namespace App\Classes\Routing;


interface IRouting
{
    public function add(string $route, string $ca);

    public function dispatch();
}