<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 15:19
 */

$router->add('', 'HomeController@index')->method('get');
$router->add('user/{id}/show', 'HomeController@show')->method('get');