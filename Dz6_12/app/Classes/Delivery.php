<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 12:22
 */

namespace App\Classes;

class Delivery extends AbstractFactoryMethod
{
    /* public function makeDelivery
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function makeDelivery($param)
    {
        $type = null;
        switch ($param) {
            case 'econom':
                $type = new EconomCar();
                break;
            case 'standart':
                $type = new StandartCar();
                break;
            case 'lux':
                $type = new LuxCar();
                break;
            default:
                $type = new StandartCar();
        }
        return $type;
    }
}