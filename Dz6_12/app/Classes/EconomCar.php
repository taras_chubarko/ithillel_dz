<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 15:31
 */

namespace App\Classes;


class EconomCar extends Delivery
{
    private $model;
    private $price;

    public function __construct()
    {
        $this->model = 'Toyota Corolla';
        $this->price = 100;
    }

    /* public function getModel
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /* public function getPrice
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPrice(): int
    {
        return $this->price;
    }
}