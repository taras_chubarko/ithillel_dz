<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 15:38
 */

namespace App\Classes;


class LuxCar
{
    private $model;
    private $price;

    public function __construct()
    {
        $this->model = 'Lamborghini Huracan';
        $this->price = 1000;
    }

    /* public function getModel
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /* public function getPrice
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPrice(): int
    {
        return $this->price;
    }
}