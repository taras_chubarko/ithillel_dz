<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 12:15
 */

namespace App\Classes;


abstract class AbstractFactoryMethod
{
    abstract function makeDelivery($param);
}