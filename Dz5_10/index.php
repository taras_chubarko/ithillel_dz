<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 06.08.2021
 * Time: 14:51
 */


class Product
{
    protected $productRepository;
    protected $product;

    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(ProductRepository $productRepository, ProductInterface $product)
    {
        $this->productRepository = $productRepository;
        $this->product = $product;
    }

    public function get()
    {
    }

    public function set()
    {
    }
}


interface ProductInterface
{
    public function show($product);

    public function print($product);
}

class PrintShowProduct implements ProductInterface
{
    public function show($product)
    {
        return '<h1>Product: ' . $product . '</h1>';
    }

    public function print($product)
    {
        return '<h1>Product: ' . $product . '</h1>';
    }
}

class ProductRepository
{
    public function save($product)
    {

    }

    public function update($product)
    {

    }

    public function delete($product)
    {
    }

}