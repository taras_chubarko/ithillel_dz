<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.07.2021
 * Time: 9:06
 */

class Worker
{
    public $name;
    public $age;
    public $salary;

}

$object1 = new Worker();
$object1->name = 'John';
$object1->age = 25;
$object1->salary = 1000;


$object2 = new Worker();
$object2->name = 'Вася';
$object2->age = 26;
$object2->salary = 2000;

$salarySum = $object1->salary + $object2->salary;
$ageSum = $object1->age + $object2->age;

echo 'сумма зарплат Ивана и Васи: ' . $salarySum . '</br>';
echo 'сумма возрастов Ивана и Васи: ' . $ageSum;

