<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.07.2021
 * Time: 11:58
 */

//Абстрактный класс — это класс, у которого не реализован один или больше методов
abstract class SomeAbstractClass
{
    private $age;

    abstract protected function getValue();

    /* public function getAge
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAge() : int
    {
        return $this->age;
    }

    /* public function setAge
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }
}

class SomeAbstractClassNew extends SomeAbstractClass
{
    protected function getValue()
    {
        // TODO: Implement getValue() method.
    }
}

//Интерфейс — это абстрактный класс, у которого ни один метод не реализован, все они публичные и нет переменных класса

interface ISome
{
    public function setVariable($var);
    public function getVariable();
}

class SomeClass implements ISome
{
    public function setVariable($var)
    {
        // TODO: Implement setVariable() method.
    }

    public function getVariable()
    {
        // TODO: Implement getVariable() method.
    }
}