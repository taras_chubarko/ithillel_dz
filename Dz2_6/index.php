<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 26.07.2021
 * Time: 9:55
 */
trait Trait1
{
    /* public function method
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function method(): int
    {
        return 1;
    }
}

trait Trait2
{
    /* public function method
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function method(): int
    {
        return 2;
    }
}

trait Trait3
{
    /* public function method
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function method(): int
    {
        return 3;
    }
}

class Test
{
    use Trait1, Trait2, Trait3 {
        Trait2::method insteadof Trait1;
        Trait3::method insteadof Trait2;
        Trait1::method as method1;
        Trait2::method as method2;
        Trait3::method as method3;
    }

    /* public function getSum
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSum(): int
    {
        $arr = [$this->method1(), $this->method2(), $this->method3()];
        return array_sum($arr);
    }
}

$obj = new Test();

echo $obj->getSum();